package cat.itb.booktrackerapp.Models;

import java.util.ArrayList;

public class BooksViewModel extends  androidx.lifecycle.ViewModel {

    public static ArrayList<BooksModel> booksArrayList = new ArrayList<>();
    BooksModel book1 = new BooksModel("Sapiens", "Yuval Noah Harari", "Read", "Kinneret Zmora-Bitan Dvir", 4.5);
    BooksModel book3 = new BooksModel("Hit Refresh", "Satya Nadella", "Read", "Harper Collins", 2.5);
    BooksModel book4 = new BooksModel("Por qué fracasan los países", "Daron Acemoglu / James A. Robinson", "Read", "Crown Publishing Group", 5.0);

    public BooksViewModel() {
        booksArrayList.add(book1);
        booksArrayList.add(book3);
        booksArrayList.add(book4);
    }
}
