package cat.itb.booktrackerapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class BooksModel implements Parcelable {

    private String title;
    private String author;
    private String status;
    private String publisher;
    private Double rating;

    public BooksModel(String title, String author, String status, String publisher, Double rating) {
        this.title = title;
        this.author = author;
        this.status = status;
        this.publisher = publisher;
        this.rating = rating;
    }

    protected BooksModel(Parcel in) {
        title = in.readString();
        author = in.readString();
        status = in.readString();
        publisher = in.readString();
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readDouble();
        }
    }

    public static final Creator<BooksModel> CREATOR = new Creator<BooksModel>() {
        @Override
        public BooksModel createFromParcel(Parcel in) {
            return new BooksModel(in);
        }

        @Override
        public BooksModel[] newArray(int size) {
            return new BooksModel[size];
        }
    };

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getSpinnerPos() {
        switch (status) {

            case "Pending":
                return 0;
            case "Reading":
                return 1;
            case "Read":
                return 2;
            default:
                return 0;
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(status);
        dest.writeString(publisher);
        dest.writeDouble(rating);
    }
}
