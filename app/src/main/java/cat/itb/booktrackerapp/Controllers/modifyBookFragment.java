package cat.itb.booktrackerapp.Controllers;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import cat.itb.booktrackerapp.Models.BooksModel;
import cat.itb.booktrackerapp.R;

import static cat.itb.booktrackerapp.Models.BooksViewModel.booksArrayList;

public class modifyBookFragment extends Fragment {

    EditText titleEditText;
    EditText authorEditText;
    EditText publisherEditText;
    Spinner statusSpinner;
    RatingBar ratingBar;
    FloatingActionButton addButton;
    BooksModel book;
    BooksModel auxBook;
    TextView ratingTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.book_modify_fragment, container, false);
        statusSpinner = v.findViewById(R.id.statusSpinner);
        titleEditText = v.findViewById(R.id.bookTitleEditText);
        authorEditText = v.findViewById(R.id.authorEditText);
        publisherEditText = v.findViewById(R.id.editTextPublisher);
        ratingBar = v.findViewById(R.id.ratingBar);
        Drawable drawable = ratingBar.getProgressDrawable();
        addButton = v.findViewById(R.id.addBookButton);
        ratingTextView = v.findViewById(R.id.ratingTextView);
        ratingTextView.setVisibility(View.INVISIBLE);
        ratingBar.setVisibility(View.INVISIBLE);


        if (getArguments() != null) {
            book = getArguments().getParcelable("book");

            final int position = getArguments().getInt("position");

            if (!book.getTitle().equals("null")) {


                titleEditText.setText(book.getTitle());
                authorEditText.setText(book.getAuthor());
                statusSpinner.setSelection(book.getSpinnerPos());
                ratingBar.setRating((float) book.getRating());
                publisherEditText.setText(String.valueOf(book.getPublisher()));

            }
            if (statusSpinner.getSelectedItemPosition() == 2) {
                ratingBar.setVisibility(View.VISIBLE);
                ratingBar.setVisibility(View.VISIBLE);
            }

            statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selection = (String) parent.getItemAtPosition(position);

                    if ((selection).equals("Read")) {
                        ratingBar.setVisibility(View.VISIBLE);
                        ratingTextView.setVisibility(View.VISIBLE);
                    } else {
                        ratingBar.setVisibility(View.INVISIBLE);
                        ratingTextView.setVisibility(View.INVISIBLE);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checkData = checkData();
                    if (checkData) {
                        loadGivenInfo();
                        if (position == -1) {
                            booksArrayList.add(auxBook);
                        } else {
                            booksArrayList.set(position, auxBook);
                        }

                        NavDirections navDirections = modifyBookFragmentDirections.actionFromModifyBookToMainFragment();
                        Navigation.findNavController(v).navigate(navDirections);
                    } else {
                        Toast.makeText(getActivity(), "Please, fill all the fields", Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }


        return v;


    }

    public boolean checkData() {
        if (titleEditText.getText().toString().equals("") || authorEditText.getText().toString().equals("") || publisherEditText.getText().toString().equals("")) {
            return false;
        } else return true;
    }

    public void loadGivenInfo() {

        double rating;


        if (ratingBar.getVisibility() == View.INVISIBLE) {
            rating = 0.0;
        } else {
            rating = ratingBar.getRating();
        }

        auxBook = new BooksModel((titleEditText.getText().toString()),
                (authorEditText.getText().toString()),
                (statusSpinner.getSelectedItem().toString()),
                publisherEditText.getText().toString(), rating
        );


    }

}
