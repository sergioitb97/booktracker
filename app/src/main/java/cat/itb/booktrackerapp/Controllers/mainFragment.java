package cat.itb.booktrackerapp.Controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import cat.itb.booktrackerapp.Models.BooksModel;
import cat.itb.booktrackerapp.Models.BooksViewModel;
import cat.itb.booktrackerapp.R;
import cat.itb.booktrackerapp.Views.BookAdapter;

import static cat.itb.booktrackerapp.Models.BooksViewModel.booksArrayList;

public class mainFragment extends Fragment {

    BooksViewModel booksViewModel;
    RecyclerView bookListItem;
    FloatingActionButton fButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        booksViewModel = new ViewModelProvider(getActivity()).get(BooksViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.main_fragment,container,false);
        bookListItem = v.findViewById(R.id.recyclerView);


        bookListItem.setLayoutManager(new GridLayoutManager(getContext(),2));
        BookAdapter adapter = new BookAdapter(booksArrayList);
        fButton  = v.findViewById(R.id.floatingButton);
        bookListItem.setAdapter(adapter);

        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BooksModel l  =new BooksModel("null","null","Reading","null",10.0);
                NavDirections navDirections = mainFragmentDirections.actionFromMainFragmentToModifyFragment(l,-1);
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

        return v;

    }



}
