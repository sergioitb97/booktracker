package cat.itb.booktrackerapp.Views;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.booktrackerapp.Controllers.mainFragmentDirections;
import cat.itb.booktrackerapp.Models.BooksModel;
import cat.itb.booktrackerapp.R;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    List<BooksModel> book;


    public BookAdapter(List<BooksModel> book) {
        this.book = book;
    }

    class BookViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView author;
        TextView status;
        TextView rating;
        TextView publisher;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.bookTitleTextView);
            author = itemView.findViewById(R.id.textViewAuthor);
            status = itemView.findViewById(R.id.statusTextView);
            rating = itemView.findViewById(R.id.ratingTextView);
            publisher = itemView.findViewById(R.id.publisherTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   NavDirections navDirections = mainFragmentDirections.actionFromMainFragmentToModifyFragment(book.get(getAdapterPosition()),getAdapterPosition());
                   Navigation.findNavController(v).navigate(navDirections);
                }
            });

        }
        @SuppressLint("SetTextI18n")
        public void bindData(BooksModel book){
            title.setText(book.getTitle());
            author.setText("Author: ".concat(book.getAuthor()));
            status.setText(("Status: ".concat(book.getStatus())));
            if (book.getStatus().equals("Read")){
                rating.setText("Rating: "+book.getRating());
            }else {
                rating.setText("");
            }

            publisher.setText("Publisher: "+book.getPublisher());
        }

    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.books_item,parent,false);
        return new BookViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(book.get(position));
    }

    @Override
    public int getItemCount() {
        return book.size();
    }
}
